Some simple code based around the Sparkfun PCF85063 Real Time Clock (RTC) Arduino library.  This PlatformIO Arduino code allows you to manually set the time, and prints out the time.

There is a (not used here) snipit of code in loop() to parse a datetime string and enter that into the RTC.

To use this you will need to change the SDA/SCL GPIO pins to suit your ESP32-S3 configuration.  This should all compile for any ESP32 based MCU, of course you will need to change the board selection to suit what your using.

I am using a RTC clock crystal that I think requires a 12pF loading capacitor.  In the code example I have selected the built in 12.5pF loading caps and I find my time is drifting by about 1.86 seconds slow per day.  It was drifting by around 11 seconds per day if I select the 7.5pF loading capacitors, which is the default selection in the Sparkfun library.

To test the time drift, I manually set the time in the code to be a minute or two ahead, then compile and upload the code to the RTC.  I then turn off the upload option by setting:  setTimeEnable = false;

Then with a second terminal window open with the CLI preloaded with :  date  I reboot the ESP32-S3 and once I have text from the Setup() - "Serial Ready", I press <Enter> key on the computer to check it's datetime and compare with the printTime() function listing via the PlatformIO terminal.  

I then check this again in ~24hrs and xx days later to work out the drift over time.
