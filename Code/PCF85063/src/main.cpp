/* #######################################################
*
* Code example Writen for ESP32-S3 MCU connected to a i2C based PCF85063 Real Time Clock (RTC)
*
* Parkview 2023 - Licensed CC
*
* view code at: https://gitlab.com/parkview/PCF85063-RTC-Example
*
*

*/

#include <Arduino.h>
#include <PCF85063TP.h> // Seeed's RTC Library: https://github.com/Seeed-Studio/Grove_High_Precision_RTC_PCF85063TP
#include <Wire.h>

#define SDA 21   // change this to suit your i2C GPIO
#define SCL 47   // change this to suit your i2C GPIO

const bool setTimeEnable = false; // if set to true, stores the current time into the RTC

PCD85063TP RTclock; // define a object of PCD85063TP class

void printTime()
{
  // this prints out a nicely formatted datetime string to the Serial Port
  RTclock.getTime();
  Serial.print(RTclock.hour, DEC);
  Serial.print(":");
  Serial.print(RTclock.minute, DEC);
  Serial.print(":");
  Serial.print(RTclock.second, DEC);
  Serial.print("  ");
  Serial.print(RTclock.year + 2000, DEC);
  Serial.print("-");
  Serial.print(RTclock.month, DEC);
  Serial.print("-");
  Serial.print(RTclock.dayOfMonth, DEC);
  Serial.print(" ");
  switch (RTclock.dayOfWeek)
  { // Friendly printout the weekday
  case MON:
    Serial.print("Monday");
    break;
  case TUE:
    Serial.print("Tuesday");
    break;
  case WED:
    Serial.print("Wednsday");
    break;
  case THU:
    Serial.print("Thursday");
    break;
  case FRI:
    Serial.print("Friday");
    break;
  case SAT:
    Serial.print("Saturday");
    break;
  case SUN:
    Serial.print("Sunday");
    break;
  }
  Serial.println(" ");
}

void setTime()
{
  // use this to manually set a datetime into PFC85063 RTC
  RTclock.stopClock();
  RTclock.fillByYMD(2023, 10, 17); // 2023-10-17 - 17th of Oct 2023
  RTclock.fillByHMS(16, 42, 00);   // 16:42 00, or 4:40pm
  RTclock.fillDayOfWeek(TUE);      // Tuesday. NOTE: the RTC can't work out the current Day of the Week. You have to manually set it
  RTclock.setTime();               // write time to the RTC chip
  RTclock.startClock();            // start the clock again
}

void setup()
{
  Wire.begin(SDA, SCL);

  Serial.begin(115200);
  Serial.setTxTimeoutMs(0);
  delay(1500); // wait till serial is online
  Serial.println("serial ready");

  RTclock.begin();                 // initialise the RTC IC
  RTclock.cap_sel(CAP_SEL_12_5PF); // by default, it's set to 7.5pH caps. Remm this line out to use those
  if (setTimeEnable)
  {
    // writes the current time to the RTC IC, then pauses
    setTime(); //  Use this to set date/time. Need to edit function with correct time first.
    Serial.print("Time has been set: ");
    printTime();
    Serial.print("set setTimeEnable to false and upload again");
    while (1)
    {
      // pause here
    }
  }
  printTime();

  Serial.println("Setup Finished!");
}

void loop()
{
  // do nothing
/*
// NOTE: this is nothing to do with the above Setup code.  
        The code below is an example from parsing a datetime string thats been entered via a webpage
  if (inputParam == "datetime")
  {
    // this will input and check the datetime string thats being inputted from the webpage to make sure it's correctly formatted
    //  and then if ok, write the data into the RTC, and then update the webpage that it's been updated ok
    String webDateTime = inputMessage;
    Serial.print("Datetime field from the webpage: ");
    Serial.println(webDateTime);
    // char WebDateTime[] = {"2023-09-27 20:37:15"};  // example string in STD format: YYYY-MM-DD hh-mm-ss
    if (webDateTime.length() != 19)
    {
      // notify webpage that there is a datetime string format error!
    }
    else
    {
      // we have a correct string format.  Go split it up and write out values to RTC
      // Split the string into substrings
      String strs[20];
      u8_t StringCount = 0;
      String strs1[20];
      u8_t StringCount1 = 0;
      String dow = "";
      while (webDateTime.length() > 0)
      {
        int index = webDateTime.indexOf('-');
        if (index == -1) // No space found
        {
          strs[StringCount++] = webDateTime;
          break;
        }
        else
        {
          strs[StringCount++] = webDateTime.substring(0, index);
          webDateTime = webDateTime.substring(index + 1);
        }
      }
      // manually force the date into the variable
      strs[StringCount++] = webDateTime.substring(0, 2); // manually add in the day field
      webDateTime = webDateTime.substring(3);            // get rid of day from string
      while (webDateTime.length() > 0)
      {
        int index = webDateTime.indexOf(':');
        if (index == -1) // No space found
        {
          strs[StringCount++] = webDateTime;
          break;
        }
        else
        {
          strs[StringCount++] = webDateTime.substring(0, index);
          webDateTime = webDateTime.substring(index + 1);
        }
        strs[StringCount++] = webDateTime; // manually add in the second field
      }
      // need a whole bunch of error checking here to somewhat verify correct data entry
      // NOTE: missing indexes below contain crappy data!
      Serial.println(strs[0]); // year
      Serial.println(strs[1]); // Month
      Serial.println(strs[3]); // Day
      Serial.println(strs[4]); // Hour
      Serial.println(strs[6]); // Minute
      Serial.println(strs[7]); // Second

      // now go store the datetime values into the RTC
      u8_t DoW = dayOfWeek(strs[0].toInt(), strs[1].toInt(), strs[3].toInt());
      Serial.print("DoW: ");
      Serial.println(DoW);
      RTclock.stopClock();
      RTclock.fillByYMD(strs[0].toInt(), strs[1].toInt(), strs[3].toInt()); // YYYY,MM,DD
      RTclock.fillByHMS(strs[4].toInt(), strs[6].toInt(), strs[7].toInt()); // hh,mm,ss
      RTclock.fillDayOfWeek(DoW);                                           // Friday
      RTclock.setTime();                                                    // write time to the RTC chip
      RTclock.startClock();                                                 // start the clock again
    }
  }
  */
}
